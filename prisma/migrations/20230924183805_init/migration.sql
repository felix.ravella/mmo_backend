/*
  Warnings:

  - Made the column `current_map` on table `Hero` required. This step will fail if there are existing NULL values in that column.
  - Made the column `position_x` on table `Hero` required. This step will fail if there are existing NULL values in that column.
  - Made the column `position_y` on table `Hero` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Hero" ADD COLUMN     "hp" INTEGER NOT NULL DEFAULT 25,
ADD COLUMN     "max_hp" INTEGER NOT NULL DEFAULT 25,
ADD COLUMN     "skill_03_xp" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN     "skill_13_xp" INTEGER NOT NULL DEFAULT 0,
ADD COLUMN     "skill_23_xp" INTEGER NOT NULL DEFAULT 0,
ALTER COLUMN "current_map" SET NOT NULL,
ALTER COLUMN "current_map" SET DEFAULT 'playground',
ALTER COLUMN "position_x" SET NOT NULL,
ALTER COLUMN "position_x" SET DEFAULT 50,
ALTER COLUMN "position_y" SET NOT NULL,
ALTER COLUMN "position_y" SET DEFAULT 50;
