-- CreateTable
CREATE TABLE "ResourceInstance" (
    "uuid" TEXT NOT NULL,
    "blueprint" TEXT NOT NULL,
    "quantity" INTEGER NOT NULL DEFAULT 1,
    "quality" INTEGER NOT NULL DEFAULT 0,
    "owner_uuid" TEXT,

    CONSTRAINT "ResourceInstance_pkey" PRIMARY KEY ("uuid")
);

-- CreateIndex
CREATE UNIQUE INDEX "ResourceInstance_uuid_key" ON "ResourceInstance"("uuid");

-- AddForeignKey
ALTER TABLE "ResourceInstance" ADD CONSTRAINT "ResourceInstance_owner_uuid_fkey" FOREIGN KEY ("owner_uuid") REFERENCES "Hero"("uuid") ON DELETE SET NULL ON UPDATE CASCADE;
