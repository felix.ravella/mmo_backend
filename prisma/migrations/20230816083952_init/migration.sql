-- CreateTable
CREATE TABLE "User" (
    "uuid" TEXT NOT NULL,
    "login" TEXT NOT NULL,
    "password" TEXT NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "Hero" (
    "uuid" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "job" TEXT NOT NULL,
    "xp" INTEGER NOT NULL DEFAULT 0,
    "current_map" TEXT,
    "position_x" INTEGER,
    "position_y" INTEGER,
    "skill_00_xp" INTEGER NOT NULL DEFAULT 0,
    "skill_01_xp" INTEGER NOT NULL DEFAULT 0,
    "skill_02_xp" INTEGER NOT NULL DEFAULT 0,
    "skill_10_xp" INTEGER NOT NULL DEFAULT 0,
    "skill_11_xp" INTEGER NOT NULL DEFAULT 0,
    "skill_12_xp" INTEGER NOT NULL DEFAULT 0,
    "skill_20_xp" INTEGER NOT NULL DEFAULT 0,
    "skill_21_xp" INTEGER NOT NULL DEFAULT 0,
    "skill_22_xp" INTEGER NOT NULL DEFAULT 0,
    "user_uuid" TEXT NOT NULL,

    CONSTRAINT "Hero_pkey" PRIMARY KEY ("uuid")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_uuid_key" ON "User"("uuid");

-- CreateIndex
CREATE UNIQUE INDEX "User_login_key" ON "User"("login");

-- CreateIndex
CREATE UNIQUE INDEX "Hero_uuid_key" ON "Hero"("uuid");

-- CreateIndex
CREATE UNIQUE INDEX "Hero_name_key" ON "Hero"("name");

-- AddForeignKey
ALTER TABLE "Hero" ADD CONSTRAINT "Hero_user_uuid_fkey" FOREIGN KEY ("user_uuid") REFERENCES "User"("uuid") ON DELETE RESTRICT ON UPDATE CASCADE;
