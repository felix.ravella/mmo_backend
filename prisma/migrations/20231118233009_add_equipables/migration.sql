-- CreateTable
CREATE TABLE "EquipableInstance" (
    "uuid" TEXT NOT NULL,
    "blueprint" TEXT NOT NULL,
    "durability" INTEGER NOT NULL DEFAULT 10,
    "quality" INTEGER NOT NULL DEFAULT 0,
    "stat_0_value" INTEGER,
    "stat_1_value" INTEGER,
    "stat_2_value" INTEGER,
    "stat_3_value" INTEGER,
    "vanish_at" DATE,
    "owner_uuid" TEXT,
    "crafter_uuid" TEXT,

    CONSTRAINT "EquipableInstance_pkey" PRIMARY KEY ("uuid")
);

-- CreateIndex
CREATE UNIQUE INDEX "EquipableInstance_uuid_key" ON "EquipableInstance"("uuid");

-- AddForeignKey
ALTER TABLE "EquipableInstance" ADD CONSTRAINT "EquipableInstance_owner_uuid_fkey" FOREIGN KEY ("owner_uuid") REFERENCES "User"("uuid") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "EquipableInstance" ADD CONSTRAINT "EquipableInstance_crafter_uuid_fkey" FOREIGN KEY ("crafter_uuid") REFERENCES "User"("uuid") ON DELETE SET NULL ON UPDATE CASCADE;
