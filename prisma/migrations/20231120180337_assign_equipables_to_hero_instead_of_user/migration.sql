-- DropForeignKey
ALTER TABLE "EquipableInstance" DROP CONSTRAINT "EquipableInstance_crafter_uuid_fkey";

-- DropForeignKey
ALTER TABLE "EquipableInstance" DROP CONSTRAINT "EquipableInstance_owner_uuid_fkey";

-- AddForeignKey
ALTER TABLE "EquipableInstance" ADD CONSTRAINT "EquipableInstance_owner_uuid_fkey" FOREIGN KEY ("owner_uuid") REFERENCES "Hero"("uuid") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "EquipableInstance" ADD CONSTRAINT "EquipableInstance_crafter_uuid_fkey" FOREIGN KEY ("crafter_uuid") REFERENCES "Hero"("uuid") ON DELETE SET NULL ON UPDATE CASCADE;
