-- mmo."User" definition

-- Drop table

-- DROP TABLE mmo."User";

CREATE TABLE mmo."User" (
	uuid text NOT NULL,
	login text NOT NULL,
	"password" text NOT NULL,
	CONSTRAINT "User_pkey" PRIMARY KEY (uuid)
);
CREATE UNIQUE INDEX "User_login_key" ON mmo."User" USING btree (login);
CREATE UNIQUE INDEX "User_uuid_key" ON mmo."User" USING btree (uuid);

-- mmo."Hero" definition

-- Drop table

-- DROP TABLE mmo."Hero";

CREATE TABLE mmo."Hero" (
	uuid text NOT NULL,
	"name" text NOT NULL,
	job text NOT NULL,
	xp int4 NOT NULL DEFAULT 0,
	current_map text NULL,
	position_x int4 NULL,
	position_y int4 NULL,
	skill_00_xp int4 NOT NULL DEFAULT 0,
	skill_01_xp int4 NOT NULL DEFAULT 0,
	skill_02_xp int4 NOT NULL DEFAULT 0,
	skill_10_xp int4 NOT NULL DEFAULT 0,
	skill_11_xp int4 NOT NULL DEFAULT 0,
	skill_12_xp int4 NOT NULL DEFAULT 0,
	skill_20_xp int4 NOT NULL DEFAULT 0,
	skill_21_xp int4 NOT NULL DEFAULT 0,
	skill_22_xp int4 NOT NULL DEFAULT 0,
	user_uuid text NOT NULL,
	CONSTRAINT "Hero_pkey" PRIMARY KEY (uuid)
);
CREATE UNIQUE INDEX "Hero_name_key" ON mmo."Hero" USING btree (name);
CREATE UNIQUE INDEX "Hero_uuid_key" ON mmo."Hero" USING btree (uuid);


-- mmo."Hero" foreign keys

ALTER TABLE mmo."Hero" ADD CONSTRAINT "Hero_user_uuid_fkey" FOREIGN KEY (user_uuid) REFERENCES mmo."User"(uuid) ON DELETE RESTRICT ON UPDATE CASCADE;



-- mmo."_prisma_migrations" definition

-- Drop table

-- DROP TABLE mmo."_prisma_migrations";

CREATE TABLE mmo."_prisma_migrations" (
	id varchar(36) NOT NULL,
	checksum varchar(64) NOT NULL,
	finished_at timestamptz NULL,
	migration_name varchar(255) NOT NULL,
	logs text NULL,
	rolled_back_at timestamptz NULL,
	started_at timestamptz NOT NULL DEFAULT now(),
	applied_steps_count int4 NOT NULL DEFAULT 0,
	CONSTRAINT "_prisma_migrations_pkey" PRIMARY KEY (id)
);
