// lists all possible battles (horde + positions) for each map

import { hordesData } from "./hordes.data.js";

export const battleDatas = {
  "playground": {
    "1": {
      id: "1",
      ...hordesData.default_01,
    },
    "2": {
      id: "2",
      ...hordesData.default_01,
    },
  },
  "01": {
    "1": {
      id: "1",
      ...hordesData.forest_01,
    },
    "2": {
      id: "2",
      ...hordesData.forest_02,
    },
  },
  "02": {
    "1": {
      id: "1",
      ...hordesData.forest_01,
    },
  },
  // column "10"
  "10,10": {
    "1": { id: "1", ...hordesData.plain_01 },
    "2": { id: "2", ...hordesData.plain_02 },
  },
  "10,11": {
    "1": { id: "1", ...hordesData.plain_01 },
    "2": { id: "2", ...hordesData.plain_02 },
  },
  "10,12": {
    "1": { id: "1", ...hordesData.plain_01 },
    "2": { id: "2", ...hordesData.plain_02 },
  },
  "10,13": {
    "1": { id: "1", ...hordesData.plain_01 },
    "2": { id: "2", ...hordesData.plain_02 },
  },
  // column "11"
  "11,10": {
    "1": { id: "1", ...hordesData.forest_01 },
    "2": { id: "2", ...hordesData.forest_02 },
    "3": { id: "3", ...hordesData.forest_06 },
    "4": { id: "4", ...hordesData.forest_07 },
  },
  "11,11": {
    "1": { id: "1", ...hordesData.forest_01 },
    "2": { id: "2", ...hordesData.forest_02 },
    "3": { id: "3", ...hordesData.forest_06 },
    "4": { id: "4", ...hordesData.forest_07 },
  },
  "11,12": {
    "1": { id: "1", ...hordesData.forest_01 },
    "2": { id: "2", ...hordesData.forest_02 },
    "3": { id: "3", ...hordesData.forest_06 },
    "4": { id: "4", ...hordesData.forest_07 },
  },
  "11,13": {
    "1": { id: "1", ...hordesData.forest_01 },
    "2": { id: "2", ...hordesData.forest_02 },
    "3": { id: "3", ...hordesData.forest_06 },
    "4": { id: "4", ...hordesData.forest_07 },
  },
  // column "12"
  "12,10": {
    "1": { id: "1", ...hordesData.forest_01 },
    "2": { id: "2", ...hordesData.forest_02 },
    "3": { id: "3", ...hordesData.forest_03 },
    "4": { id: "4", ...hordesData.forest_06 },
  },
  "12,11": {
    "1": { id: "1", ...hordesData.forest_01 },
    "2": { id: "2", ...hordesData.forest_02 },
    "3": { id: "3", ...hordesData.forest_02 },
    "4": { id: "4", ...hordesData.forest_07 },
  },
  "12,12": {
    "1": { id: "1", ...hordesData.forest_01 },
    "2": { id: "2", ...hordesData.forest_01 },
    "3": { id: "3", ...hordesData.forest_02 },
    "4": { id: "4", ...hordesData.forest_06 },
  },
  "12,13": {
    "1": { id: "1", ...hordesData.forest_02 },
    "2": { id: "2", ...hordesData.forest_03 },
  },
  // column "13"
  "13,10": {
    "1": { id: "1", ...hordesData.forest_02 },
    "2": { id: "2", ...hordesData.forest_03 },
    "3": { id: "3", ...hordesData.forest_06 },
  },
  "13,11": {
    "1": { id: "1", ...hordesData.forest_02 },
    "2": { id: "2", ...hordesData.forest_03 },
    "3": { id: "3", ...hordesData.forest_04 },
  },
  "13,12": {
    "1": { id: "1", ...hordesData.forest_03 },
    "2": { id: "2", ...hordesData.forest_04 },
  },
  "13,13": {
    "1": { id: "1", ...hordesData.forest_01 },
    "2": { id: "2", ...hordesData.forest_02 },
    "3": { id: "3", ...hordesData.forest_03 },
    "4": { id: "4", ...hordesData.forest_06 },
    "5": { id: "5", ...hordesData.forest_07 },

  },
  // column "14"
  "14,11": {
    "1": { id: "1", ...hordesData.forest_boss },
  },
  "14,12": {
    "1": { id: "1", ...hordesData.forest_03 },
    "2": { id: "2", ...hordesData.forest_04 },
    "3": { id: "3", ...hordesData.forest_04 },
  }
}