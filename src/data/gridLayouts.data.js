export const gridLayouts = {
  forest_01: {
    playerCells: [
      {x: 0, y: 8},
      {x: 1, y: 9},
      {x: 2, y: 9},
      {x: 6, y: 10},
    ],
    monsterCells: [
      {x: 2, y: 0},
      {x: 3, y: 1},
      {x: 4, y: 1},
      {x: 5, y: 0},
    ],
    // TODO add obstacles and decoration
  },
  forest_02: {
    playerCells: [
      {x: 2, y: 6},
      {x: 3, y: 5},
      {x: 4, y: 4},
      {x: 5, y: 5},
      {x: 6, y: 6},
    ],
    monsterCells: [
      {x: 7, y: 0},
      {x: 8, y: 0},
      {x: 9, y: 0},
      {x: 10, y: 0},
    ],
    // TODO add obstacles and decoration
  },
  forest_03: {
    playerCells: [
      {x: 2, y: 6},
      {x: 3, y: 5},
      {x: 4, y: 4},
      {x: 5, y: 5},
      {x: 6, y: 6},
    ],
    monsterCells: [
      {x: 7, y: 1},
      {x: 8, y: 2},
      {x: 9, y: 3},
      {x: 9, y: 1},
      {x: 10, y: 2},
      {x: 11, y: 1},
    ],
    // TODO add obstacles and decoration
  }

}