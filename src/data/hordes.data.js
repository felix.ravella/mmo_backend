import { gridLayouts } from "./gridLayouts.data.js";

export const hordesData = {
  default_01: {
    horde: [
      { id: "spider", level: 1 },
      { id: "spider", level: 1 },
    ],
    ...gridLayouts.forest_01,
  },
  forest_01: {
    horde: [
      { id: "spider", level: 1 },
      { id: "wasp", level: 1 },
    ],
    ...gridLayouts.forest_01,
  },
  forest_02: {
    horde: [
      { id: "wolf_pup", level: 1 },
      { id: "wolf", level: 3 },
    ],
    ...gridLayouts.forest_02,
  },
  forest_03: {
    horde: [
      { id: "wolf", level: 3 },
      { id: "wolf", level: 3 },
    ],
    ...gridLayouts.forest_01,
  },
  forest_04: {
    horde: [
      { id: "spider", level: 1 },
      { id: "wolf", level: 3 },
      { id: "wolf", level: 3 },
      { id: "wolf_pup", level: 1 },
    ],
    ...gridLayouts.forest_02,
  },
  forest_06: {
    horde: [
      { id: "wolf_pup", level: 1},
    ],
    ...gridLayouts.forest_01,

  },
  forest_07: {
    horde: [
      { id: "wolf_pup", level: 1},
      { id: "wolf_pup", level: 1},
    ],
    ...gridLayouts.forest_02,
  },
  forest_boss: {
    horde: [
      { id: "wolf_pup", level: 1 },
      { id: "wolf", level: 3 },
      { id: "white_wolf", level: 6 },
      { id: "wolf", level: 3 },
      { id: "wolf_pup", level: 1 },
    ],
    ...gridLayouts.forest_03,
  },
  plain_01: {
    horde: [
      { id: "spider", level: 1 },
      { id: "spider", level: 1 },
      { id: "wasp", level: 1 },
    ],
    ...gridLayouts.forest_01,
  },
  plain_02: {
    horde: [
      { id: "wasp", level: 1 },
      { id: "wasp", level: 1 },
      { id: "wasp", level: 1 },
    ],
    ...gridLayouts.forest_02,
  }
}