import bodyParser from 'body-parser';

import { findUserByLogin } from '../repositories/users.repository.js';

export function postConnect(app) {
  app.post(
    '/connect',
    bodyParser.json(),
    async (req, res) => {
      const body = req.body; 
      
      const user = await findUserByLogin(body.login);
      if (!user) {
        res.status(404).send("No user found");
        return;
      }

      res.status(200).send(user);
    });
}