import { nanoid } from 'nanoid';

import { broadcast } from '../scripts/broadcast.js';
import { battleDatas } from '../data/battles.data.js';
import { checkEveryBattlersReady, assignLoot, getFirstAvailableCell, getRandomSeed, getTotalLoot, isCellFree } from '../scripts/utils.js';
import { findHeroByUUID, updateHero } from '../repositories/heroes.repository.js';
import { addResourcesToHero } from '../repositories/resourceInstances.repository.js';

/*
type UserMessage = {
  author: Record<string, any>,
  type: string,
  content: any
}
*/

export function getWs(app, sockets, maps) {
  app.ws('/ws/:playerUUID',
    async (ws, req) => {
      

      const playerUUID = req.params.playerUUID;
      console.log("ws connect from", playerUUID)
      sockets.set(`${playerUUID}`, ws);

      let playerData = await findHeroByUUID(playerUUID)
      // if couldn't find player, close ws client
      if (playerData == null) {
        ws.close();
        return;
      }
      // if player has not current map, update to 'playground'
      if (!playerData.current_map) {
        playerData = await updateHero(playerUUID, {current_map: "playground", position_x: 50, position_y: 50})
      }
      // add player to starting room (playground for now, later should be retrieved from DB)
      const currentMapId = playerData.current_map
      maps[currentMapId].players.set(`${playerUUID}`, playerData)
      const playerJoinMessage = { type: "player_joined", content: { uuid: `${playerUUID}`, data: playerData }}
      broadcast(maps[currentMapId], sockets, playerJoinMessage);
      ws.send(JSON.stringify(
        {
          type: "connection_granted",
          content: {
            map: currentMapId
          },
        }
      ));

      // handle messages from sockets
      ws.on('message', async (payload) => {
        const data = JSON.parse(payload)
        if (data.type !== 'heartbeat') {
          console.log('message received:', data.type)
        }

        switch (data.type) {
          case "chat_message": // content: message: String
          {
            const playerUUID = data.author.uuid;
            const currentMapId = data.author.current_map;
            const messageData = data.content;
            const chatMessageSentMessage = { type: "chat_message", content: { uuid: `${playerUUID}`, message_data: messageData } };
            
            // limit to map
            /*
            broadcast(maps[currentMapId], sockets, chatMessageSentMessage, [playerUUID]);
            */

            // global
            sockets.forEach((socket, uuid) => {
              if (uuid != playerUUID)
              socket.send(JSON.stringify(chatMessageSentMessage));
            })
            
            break;
          }
          case "change_map":  // content: {destination_map: String, destination_position: {x,y}}
          {
            const currentMapId = data.author.current_map;
            const playerUUID = data.author.uuid;
            // save data to transfer to new map
            let playerData = Object.assign(maps[currentMapId].players.get(playerUUID));

            // add player to server state
            maps[currentMapId].players.delete(playerUUID);
            // broadcast other players that author left
            const playerLeftMessage = { type: "player_left", content: `${playerUUID}` }
            broadcast(maps[currentMapId], sockets, playerLeftMessage)

            const newMapId = data.content.destination_map;

            // TODO set new position on map
            playerData.position_x = data.content.destination_position.x;
            playerData.position_y = data.content.destination_position.y;
            maps[newMapId].players.set(`${playerUUID}`, playerData);

            // record changes in DB
            const newLocationData = {current_map: newMapId, position_x: playerData.position_x, position_y: playerData.position_y}
            await updateHero(playerUUID, newLocationData)

            // broadcast other players that author joined
            const playerJoinMessage = { type: "player_joined", content: { uuid: `${playerUUID}`, data: playerData } }
            broadcast(maps[newMapId], sockets, playerJoinMessage);
            ws.send(JSON.stringify({type: "map_joined",content: `${newMapId}`}));
            break;
          }
          case "move_player":
          {
            const playerUUID = data.author.uuid;
            const currentMapId = data.author.current_map;
            // update positions in Map

            maps[currentMapId].players.get(playerUUID).position_x = data.content.x;
            maps[currentMapId].players.get(playerUUID).position_y = data.content.y;
            // update position in DB
            const newCoordsData = {position_x: data.content.x, position_y: data.content.y};
            await updateHero(playerUUID, newCoordsData);
            // broadcast to others
            const playerMovedMessage = { type: "player_moved", content: { uuid: `${playerUUID}`, coords: data.content } }
            broadcast(maps[currentMapId], sockets, playerMovedMessage)
            break;
          }
          case "create_lobby": // content: hordeUUID
          {
            const authorUUID = data.author.uuid;
            const currentMapId = data.author.current_map;
            const hordeUUID = data.content;
            const hordeData = maps[currentMapId].hordes.get(hordeUUID);
            const currentMap = maps[currentMapId];
            const authorData = Object.assign(currentMap.players.get(authorUUID))

            // remove horde
            currentMap.hordes.delete(hordeUUID)
            // broadcast horde removed
            const hordeRemovedMessage = { type: "horde_removed", content: `${hordeUUID}`}
            broadcast(currentMap, sockets, hordeRemovedMessage);
            // add battle to map

            let players = new Map();
            let hero = await findHeroByUUID(authorUUID);
            players.set(authorUUID, {
              uuid: authorUUID,
              battler_data: { 
                "name": hero.name,
                "sprite": hero.job,
                "max_ap": 6, "max_mp": 3,
                "max_hp": hero.max_hp,
                "skills": [],
              },
              hero_data: hero,
              position: {...battleDatas[currentMapId][hordeData.id].playerCells[0]},
              isReady: false,
            }) // temp, should retrieve from db

            const battleUUID = nanoid();
            for (let i = 0; i < hordeData.horde.length; i++) {
              hordeData.horde[i]["position"] = {...hordeData.monsterCells[i]}
            };

            let battleData = {
              horde: hordeData.horde,
              players: Object.fromEntries(players),
              player_cells: battleDatas[currentMapId][hordeData.id].playerCells,
              monster_cells: battleDatas[currentMapId][hordeData.id].monsterCells,
              createdAt: 0, // timestamp for joining players to know remaining time
              status: "open", // status is open | locked | playing
              map: currentMapId,
            }
            currentMap.battles.set(battleUUID, battleData) 

            // send lobby_granted to message author, removes it from map
            ws.send(JSON.stringify({
              type: "battle_granted",
              content: {
                uuid: `${battleUUID}`, 
                battleData,
                team: 0,  // team of initiator is 0 since it is PvE
              }
            }));
            currentMap.battles.get(battleUUID).players = players;
            currentMap.players.delete(authorUUID);
            
            // broadcast author left & battle added messages
            const battleAddedMessage = { 
              type: "battle_added", 
              content: {
                uuid: `${battleUUID}`, 
                battleData: {
                  horde: hordeData, 
                  players,
                }
              } 
            }
            const playerLeftMessage = { type: "player_left", content: `${authorUUID}` }
            broadcast(currentMap, sockets, playerLeftMessage, [authorUUID]);
            broadcast(currentMap, sockets, battleAddedMessage, [authorUUID]);

            break;
          }
          case "join_lobby": // content: battleUUID
          {
            const authorUUID = data.author.uuid;
            const currentMapId = data.author.current_map;
            const lobbyUUID = data.content;
            const currentMap = maps[currentMapId];
            const currentLobby = currentMap.battles.get(lobbyUUID)
            const authorData = Object.assign(currentMap.players.get(authorUUID))

            // refuse if battle is full or invalid status (just in case)
            if (currentLobby.status === "locked") {
              ws.send(JSON.stringify({ type: "game_message", content: "Cannot join: battle is locked" }));
              return;
            }
            if (currentLobby.status === "playing") {
              ws.send(JSON.stringify({ type: "game_message", content: "Cannot join: battle has already started" }));
              return;
            }
            if (currentLobby.players.size >= currentLobby.player_cells.length) {
              ws.send(JSON.stringify({ type: "game_message", content: "Cannot join: battle is full" }));
              return;
            }
            
            const availableCell = getFirstAvailableCell(currentLobby)

            // remove author and add it to battle_lobby
            // TODO get data from Database
            const hero = await findHeroByUUID(authorUUID);
            const battlerData = { 
              "name": hero.name,
              "sprite": hero.job,
              "max_ap": 6, "max_mp": 3,
              "max_hp": hero.max_hp,
              "skills": [],
            };
            currentLobby.players.set(authorUUID, {
              uuid: authorUUID,
              battler_data: battlerData,
              hero_data: hero,
              position: {...availableCell},
              isReady: false,
            })
            
            currentMap.players.delete(authorUUID);

            // broadcast to map that author has left
            const playerLeftMessage = { type: "player_left", content: `${authorUUID}` }
            broadcast(currentMap, sockets, playerLeftMessage)

            // broadcast to battle that author has joined
            const playerJoinedLobbyMessage = { 
              type: "player_joined_lobby", 
              content: {
                uuid: `${authorUUID}`, battler_data: battlerData, hero_data: hero, position: availableCell, team: 0,
              },
            }
            broadcast(currentLobby, sockets, playerJoinedLobbyMessage)
            let lobbyData = {
              horde: currentLobby.horde,
              players: Object.fromEntries(currentLobby.players),
              player_cells: currentLobby.player_cells,
              monster_cells: currentLobby.monster_cells,
              createdAt: 0, // timestamp for joining players to know remaining time
              status: currentLobby.status // status is open | locked | playing
            }
            // send to author that battle was joined successfully
            const joinedLobbyMessage = { type: "lobby_joined", content: {
              uuid: `${lobbyUUID}`, 
              battleData: lobbyData,
              team: 0,  // TODO decide team for PvP
            }}
            ws.send(JSON.stringify(joinedLobbyMessage))
            break;
          }
          case 'change_start_position':
          {
            const authorUUID = data.author.uuid;
            const currentMap = maps[data.author.current_map];
            const battleUUID = data.content.battle_uuid;
            const currentBattle = currentMap.battles.get(battleUUID);
            const targetCell = data.content.target;
            // check if position hasn't been taken
            if (!isCellFree(targetCell, currentBattle)){
              ws.send(JSON.stringify({ type: "game_message", content: "Cannot change position: cell is taken" }));
              return;
            }
            // change position in battle
            currentBattle.players.get(authorUUID).position = targetCell;
            // broadcast to others (player already did it)
            const playerChangedPositionMessage = {
              type: "player_change_start_position",
              content: {
                uuid: authorUUID,
                target: targetCell,
              },
            }
            broadcast(currentBattle, sockets, playerChangedPositionMessage)
            break;
          }
          case 'set_ready_status': // content: {battle_uuid: String, value: boolean}
          {
            const authorUUID = data.author.uuid;
            const currentMap = maps[data.author.current_map];
            const battleUUID = data.content.battle_uuid;
            const currentBattle = currentMap.battles.get(battleUUID);
            currentBattle.players.get(authorUUID).isReady = data.content.value;
            // check if everyone is ready
            // if yes, broadcast start battle
            // if false, broadcast new isReady value to battle
            let responseMessage = {}
            if (checkEveryBattlersReady(currentBattle)) {
              currentBattle.status = "playing"
              responseMessage = {type: "battle_started", content: {"seed": getRandomSeed()}};
              const battleRemovedMessage = {type: "battle_removed", content: battleUUID};
              broadcast(currentMap, sockets, battleRemovedMessage);
            } else {
              responseMessage = {type: "player_changed_ready", content: {uuid: authorUUID, value: data.content.value}};
            }
            broadcast(currentBattle, sockets, responseMessage);
            break;
          }
          case 'move_battler': // content: {battle_uuid: String, battler_uuid: String, destination: {x,y}}
          {
            const authorUUID = data.author.uuid;
            const currentMap = maps[data.author.current_map];
            const battleUUID = data.content.battle_uuid;
            const currentBattle = currentMap.battles.get(battleUUID);
            // broadcast to all except author
            const movedMessage = {type: "battler_moved", content: {battler_uuid: data.content.battler_uuid, destination: data.content.destination} };
            broadcast(currentBattle, sockets, movedMessage, [authorUUID]);
            break;
          }
          case 'launch_skill':  // content: {battle_uuid: String, battler_uuid: String, skill_id: String, target: {x,y}, random_seed: Int}
          {
            const authorUUID = data.author.uuid;
            const currentMap = maps[data.author.current_map];
            const battleUUID = data.content.battle_uuid;
            const currentBattle = currentMap.battles.get(battleUUID);
            // broadcast to all except author
            const launchSkillMessage = {
              type: "battler_launched_skill", 
              content: {
                battler_uuid: data.content.battler_uuid,
                skill_id: data.content.skill_id, 
                target: data.content.target,
                random_seed: data.content.random_seed,
              }};
            broadcast(currentBattle, sockets, launchSkillMessage, [authorUUID]);
            break;
          }
          case 'end_turn': // content: {battle_uuid: String, battler_uuid: String}
          {
            const authorUUID = data.author.uuid;
            const currentMap = maps[data.author.current_map];
            const battleUUID = data.content.battle_uuid;
            const currentBattle = currentMap.battles.get(battleUUID);
            // broadcast to all except author 
            const endTurnMessage = {type: "turn_ended", content: {battler_uuid: data.content.battler_uuid} }
            broadcast(currentBattle, sockets, endTurnMessage, [authorUUID]);
            break;
          }
          case 'battle_ended': // content: {battle_uuid: String, result: "win" | "lose", dead_monsters: [datas], player_hps: {uuid: value} }
          {
            const currentMap = maps[data.author.current_map];
            const battleUUID = data.content.battle_uuid;
            const currentBattle = currentMap.battles.get(battleUUID);
            // store battlers uuids
            const battlerUUIDs = Object.keys(Object.fromEntries(currentBattle.players));
            // compute xp gain depending on dead monsters and loot

            const totalXpGain = data.content.dead_monsters.reduce((stack, current) => {
              return stack + current.exp;
            }, 0);
            const totalLoot = getTotalLoot(data.content.dead_monsters);
            const finalLoot = assignLoot(totalLoot, battlerUUIDs);
            
            
            for (let heroUUID in data.content.player_hps) {
              let newHP = data.content.player_hps[heroUUID];
              if (newHP == 0) {
                newHP = 1;
              }
              await updateHero(heroUUID, {hp: newHP});
              // record loot for each hero
              addResourcesToHero(heroUUID, finalLoot[heroUUID])
            }

            // add battlers to current map, and records new inventories
            for (let battlerUUID of battlerUUIDs){
              const playerData = await findHeroByUUID(battlerUUID);
              // record new inventories states in database

              currentMap.players.set(`${battlerUUID}`, playerData );
            }
            // emit to battlers that battle ended (+ loot, xp for each: client instance will sort it out). 
            // + result: instance will adapt with lose (death + go to resurection map) or win (win results, same map)
            const battleEndedMessage = {
              type: "battle_ended",
              content: {
                result: data.content.result,
                loot: finalLoot,
                xp: totalXpGain,
              }
            }
            console.log(battleEndedMessage)

            broadcast(currentBattle, sockets, battleEndedMessage);

            // delete battle after 5 second (give time to broadcast)
            setTimeout(() => {
              currentMap.battles.delete(battleUUID);
            }, 5);
            

            break;
          }
          case 'heartbeat': // content: null
          {
            ws.send(JSON.stringify({ type: "heartbeat", content: null }));
            break;
          }
          default:
            console.error("Received unexpected message of type ", data.type);
        }

      })

      ws.on('close', () => {
        let playerUUID = null;
        sockets.forEach((socket, accountUUID) => {
          if (socket === ws) {
            playerUUID = accountUUID;
          }
        })

        for (const [mapId, map] of Object.entries(maps)) {
          if (map.players.delete(playerUUID)) {
            const playerLeftMessage = { type: "player_left", content: `${playerUUID}` }
            broadcast(map, sockets, playerLeftMessage);
            return;
          }
          // TODO handle player leaving battles
        }
      })
    });
}