
import { getRandomSeed } from '../scripts/utils.js';

export function getSeed(app) {
  app.get('/randomSeed',(req, res) => {
    const seed = getRandomSeed();

    res.status(200).send(`${seed}`);
  });
}