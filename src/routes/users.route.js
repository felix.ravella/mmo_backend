import bodyParser from 'body-parser';

import { getUserPlayers, postUser } from '../controllers/users.controller.js';

export function usersRoute(app) {

  //app.get('/users/:id', getUser);
  app.get('/users/:uuid/players', getUserPlayers)

  app.post('/users', bodyParser.json(), postUser);
  /*
  app.patch('/users/:id', [bodyParser.json(), authenticationMiddleware, isSameAccountMiddleware], patchUser);
  app.delete('/users/:id', [authenticationMiddleware, isSameAccountMiddleware], deleteUser);
  */
}