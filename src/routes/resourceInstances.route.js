import bodyParser from 'body-parser';
import { deleteResourceInstance, patchResourceInstance, postResourceInstance } from '../controllers/resourceInstances.controller.js';

export function resourceInstancesRoute(app) {
  app.post('/resources', bodyParser.json(), postResourceInstance);
  app.patch('/resources/:uuid', bodyParser.json(), patchResourceInstance);
  app.delete('/resources/:uuid', bodyParser.json(), deleteResourceInstance);
}