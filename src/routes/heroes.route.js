import bodyParser from 'body-parser';

import { getPlayer, patchHero, postHero } from '../controllers/heroes.controller.js';

export function heroesRoute(app) {

  app.get('/players/:uuid', getPlayer);
  
  app.post('/players', bodyParser.json(), postHero);

  app.patch('/players/:uuid', bodyParser.json(), patchHero);
  /*
  app.delete('/users/:uuid', [authenticationMiddleware, isSameAccountMiddleware], deleteUser);
  */
}