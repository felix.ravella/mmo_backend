import bodyParser from 'body-parser';
import { postEquipableInstance } from '../controllers/equipableInstances.controller.js';

export function equipableInstancesRoute(app) {
  app.post('/equipables', bodyParser.json(), postEquipableInstance);
}