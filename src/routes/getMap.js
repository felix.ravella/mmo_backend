
export function getMap(app, maps) {
  app.get('/maps/:id',(req, res) => {

    const id = req.params.id;
    const map = maps[id];
    if (!map) {
      res.status(404).send("No map with id " + id);
    }

    const players = Object.fromEntries(map.players);

    const hordes = Object.fromEntries(map.hordes);

    const battles = Object.fromEntries(map.battles);

    res.status(200).send({players, hordes, battles});

  });
}