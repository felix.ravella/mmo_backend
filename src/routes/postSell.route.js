import bodyParser from 'body-parser';
import { findHeroByUUID, updateHero } from '../repositories/heroes.repository.js';
import { destroyManyResourceInstances, updateResourceInstance } from '../repositories/resourceInstances.repository.js';
import { destroyManyEquipableInstances } from '../repositories/equipableInstances.repository.js';


export function postSell(app) {
  app.post(
    '/sell',
    bodyParser.json(),
    async (req, res) => {
      try {

        const { hero_uuid, new_money, sold_items } = req.body;
        // collects and delete all resource instances whose stack was fully sold
        const deleteResult = await destroyManyResourceInstances(sold_items.deleted_resource_uuids)
        const deleteEquipablesResult = await destroyManyEquipableInstances(sold_items.deleted_equipable_uuids)

        for (let resource of sold_items.resources) {
          await updateResourceInstance(resource.uuid, {quantity: resource.new_quantity})
        }
      
        const hero = await updateHero(hero_uuid, {money: new_money});

        res.status(200).send(hero);
        return;
      } catch (error) {
        console.log(error);
        res.status(500).send("Internal server error");
        return;
      }
    });
}