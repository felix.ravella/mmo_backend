import { nanoid } from 'nanoid';
import { createEquipable } from "../repositories/equipableInstances.repository.js";

export function getFirstAvailableCell(lobby) {
  let players = Object.fromEntries(lobby.players);

  for (let cell of lobby.player_cells ) {
    let isCellAvailable = true
    for (let playerUUID in players){
      
      const player = players[playerUUID]
      if (cell.x === player.position.x && cell.y === player.position.y) {
        isCellAvailable = false;
      }
    }
    if (isCellAvailable) {
      return cell;
    }
  }
  
  return null;
}

export function isCellFree(cell, lobby) {
  let players = Object.fromEntries(lobby.players);
  for (let playerUUID in players){
    const player = players[playerUUID]
    if (cell.x == player.position.x && cell.y == player.position.y) {
      return false;
    }
  }
  return true
}

export function checkEveryBattlersReady(lobby) {
  let players = Object.fromEntries(lobby.players);
  for (let playerUUID in players){
    const player = players[playerUUID]
    if (!player.isReady) {
      return false;
    }
  }
  return true;
}

export function getRandomSeed(){
  let seed = 0
  for (let i = 0; i < 10; i++) {
    seed += Math.floor(10 * Math.random()) * Math.pow(10, i)
  }
  return seed;
}

/*
type Monster = {
  id: string,
  loot: Record<string, number>,
  battler_data: any,
  boosts_per_lvl: any,
  xp: number,
  behavior: string
}
*/

export function getTotalLoot(monsters) {
  const totalLoot = [];
  for (let monster of monsters) {
    for (const [blueprint, ratio] of Object.entries(monster.loot)) {
      // roll quantity
      let quantity = 0;
      // @ts-ignore
      let chance = ratio % 1.0;
      // @ts-ignore
      let rollNumber = 1 + Math.floor(chance);
      let quality = getRandomQuality();
      for (let i = 0; i < rollNumber; i++) {
        const roll = Math.random();
        quantity += (roll < chance) ? 1 : 0;
      }
      if (quantity > 0) {
        var indexAt = hasItemAtIndex(totalLoot, blueprint, quality);
        if (indexAt == -1) {
          const newItem = {blueprint, quantity, quality};
          totalLoot.push(newItem)
        } else {
          totalLoot[indexAt].quantity += quantity;
        }
      }
    }
  }
  return totalLoot;
}

export function getRandomQuality() {
  const roll = Math.random();
  if (roll < 0.03) {
    return 2;
  }
  if (roll < 0.15) {
    return 1;
  }
  return 0;
}

export function hasItemAtIndex(list, blueprint, quality) {
  for (let i = 0; i < list.length; i++) {
    if (list[i].blueprint == blueprint && list[i].quality == quality) {
      return i;
    }
  }
  return -1;
}

export function assignLoot(loot, heroUUIDs) {
  // initalize result item
  let finalLoot = {};
  heroUUIDs.map(uuid => finalLoot[uuid] = []);
  for (const item of loot) {
    for (let i = 0; i < item.quantity; i++) {
      let randomUUID = heroUUIDs[Math.floor(Math.random() * heroUUIDs.length)];
      const indexAt = hasItemAtIndex(finalLoot[randomUUID], item.blueprint, item.quality);
      if (indexAt == -1) {
        const newItem = {blueprint: item.blueprint, quantity: 1, quality: item.quality};
        finalLoot[randomUUID].push(newItem);
      } else {
        finalLoot[randomUUID][indexAt].quantity += 1;
      }
    }
  }

  return finalLoot;
}


// temporary to have basic stuff on character creation
export async function addStartingEquipables(hero_uuid) {
  let data = {
    blueprint: "wasp_armor", owner_uuid: hero_uuid, uuid: nanoid(),
    stat_0_value: getRoll(2, 5), 
    stat_1_value: getRoll(2, 7),
    stat_2_value: getRoll(3, 10),
  }
  await createEquipable(data);
  data = {
    blueprint: "wasp_helmet", owner_uuid: hero_uuid, uuid: nanoid(),
    stat_0_value: getRoll(3, 10), 
    stat_1_value: getRoll(1, 1),
    stat_2_value: getRoll(2, 4),
  }
  await createEquipable(data);
  data = {
    blueprint: "spider_corset", owner_uuid: hero_uuid, uuid: nanoid(),
    stat_0_value: getRoll(2, 4), 
    stat_1_value: getRoll(2, 5),
    stat_2_value: getRoll(1, 2),
  }
  await createEquipable(data);
  data = {
    blueprint: "spider_mask", owner_uuid: hero_uuid, uuid: nanoid(),
    stat_0_value: getRoll(3, 6), 
    stat_1_value: getRoll(2, 5),
    stat_2_value: getRoll(1, 2),
  }
  await createEquipable(data);
  data = {
    blueprint: "spider_ring", owner_uuid: hero_uuid, uuid: nanoid(),
    stat_0_value: getRoll(2, 4), 
    stat_1_value: getRoll(1, 2),
    stat_2_value: getRoll(1, 4),
  }
  await createEquipable(data);
  data = {
    blueprint: "wooden_armor", owner_uuid: hero_uuid, uuid: nanoid(),
    stat_0_value: getRoll(5, 8), 
    stat_1_value: getRoll(2, 4),
    stat_2_value: getRoll(-5, -2),
  }
  await createEquipable(data);
  data = {
    blueprint: "wooden_helmet", owner_uuid: hero_uuid, uuid: nanoid(),
    stat_0_value: getRoll(2, 5), 
    stat_1_value: getRoll(3, 5),
    stat_2_value: getRoll(-5, -2),
  }
  await createEquipable(data);
  data = {
    blueprint: "wooden_boots", owner_uuid: hero_uuid, uuid: nanoid(),
    stat_0_value: getRoll(2, 5), 
    stat_1_value: getRoll(3, 5),
    stat_2_value: getRoll(-5, -2),
  }
  await createEquipable(data);
}

export function getRoll(min, max) {
  return Math.floor(Math.random() * (max - min) + min)
}