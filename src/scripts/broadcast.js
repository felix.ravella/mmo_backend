// broadcast a message to all players in a map or a battle
// the except argument is the array of users uuids NOT to send message
export function broadcast(room, sockets, message, excepts = []) {
  room.players.forEach((user, uuid) => {
    // ignore all excepts
    if (excepts.includes(uuid)){
      return;
    }
    sockets.get(uuid).send(JSON.stringify(message))
  });
}