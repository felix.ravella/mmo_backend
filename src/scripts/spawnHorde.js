import { WebSocket } from "ws";
import { broadcast } from "./broadcast.js";
import { nanoid } from 'nanoid';

import { battleDatas } from "../data/battles.data.js";

const SPAWN_TIMER = 1000;
const MAX_HORDE_PER_MAP = 3;

const bossMaps = ["14,11"]

let timerCount = 0;

export function setSpawnHordeTimer(maps, sockets) {
  setTimeout(() => {
    timerCount += 1;
    // re-launch timer
    setSpawnHordeTimer(maps, sockets)
    spawnRandomHorde(maps, sockets);
    
  }, SPAWN_TIMER);
}

function spawnRandomHorde(maps, sockets) {
  // get random room
  var keys = Object.keys(maps)
  var randomKey = keys[ Math.floor(keys.length * Math.random())]
  // @ts-ignore
  var randomMap = maps[randomKey];

  if (!battleDatas[randomKey]) {
    return;
  }
  if (Object.keys(battleDatas[randomKey]).length == 0) {
    return;
  }
  if (randomMap.hordes.size >= MAX_HORDE_PER_MAP) {
    // do not generate since max hordes was reached for map
    return;
  }
  if (bossMaps.includes(randomKey) && randomMap.hordes.size >= 1) {
    // only 1 horde boss maps
    return;
  }
  else {
    // effectively create group
    const newUUID = nanoid()
    // TODO define random position
    const hordeData = generateHorde(randomKey);
    randomMap.hordes.set(newUUID, hordeData)
    //broadcast (with only horde uuid, battledata id, list of monsters)
    const hordeCreatedMessage = { type:"horde_added", content: {uuid: newUUID, data: {id: hordeData.id, horde: hordeData.horde}} }
    broadcast(randomMap, sockets, hordeCreatedMessage)
  }
}

export function generateHorde(mapUUID) {
  var battles = battleDatas[mapUUID];
  var keys = Object.keys(battles);
  var randomKey = keys[ Math.floor(keys.length * Math.random())]

  return battles[randomKey];
}