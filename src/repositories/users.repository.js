import { prisma } from "./prisma.js";


export async function findUserByUUID(uuid) {
  const result = await prisma.user.findUnique({
    where: {uuid},
    include: {
      heroes: true,
    }
  });
  return result;
}

export async function findUserByLogin(login) {
  const result = await prisma.user.findUnique({
    where: {login},
    include: {
      heroes: true,
    }
  })
  return result;
}

export async function createUser(payload){
  const result = await prisma.user.create({
    data: payload 
  });

  return result;
}