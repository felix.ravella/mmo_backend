import { prisma } from "./prisma.js";

export async function findEquipablesPerHeroUUID(hero_uuid) {
  const result = await prisma.equipableInstance.findMany({
    where: {owner_uuid: hero_uuid },
  });
  return result;
}

export async function createEquipable(data) {
  const result = await prisma.equipableInstance.create({
    data,
  });
  return result;
}

export async function updateEquipable(uuid, data) {
  const result = await prisma.equipableInstance.update({
    where: {uuid},
    data,
  })
  return result;
}

export async function destroyManyEquipableInstances(uuids) {
  const result = await prisma.equipableInstance.deleteMany({
    where: {
      uuid: {
        in: uuids,
      },
    },
  });
  return result;
}