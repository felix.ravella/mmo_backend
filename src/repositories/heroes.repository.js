import { prisma } from "./prisma.js";

export async function findHeroByUUID(uuid) {
  const result = await prisma.hero.findUnique({
    where: { uuid },
    include: {
      equipables: true,
      resources: true,
    }
  })
  return result;
}

export async function findHeroByName(name) {
  const result = await prisma.hero.findUnique({
    where: { name },
  })
  return result;
}

export async function findHeroesByUserUUID(userUUID) {
  const result = await prisma.hero.findMany({
    where: { user_uuid: userUUID },
  })
  return result;
}

export async function createHero(payload) {
  const result = await prisma.hero.create({
    data: payload,
  })
  return result;
}

export async function updateHero(uuid, payload) {
  const result = await prisma.hero.update({
    where: { uuid },
    data: payload,
    include: {
      equipables: true,
      resources: true,
    }
  })
  return result;
}

