import { hasItemAtIndex } from "../scripts/utils.js";
import { prisma } from "./prisma.js";
import { nanoid } from 'nanoid';


export async function findResourcesPerHeroUUID(hero_uuid) {
  const result = await prisma.resourceInstance.findMany({
    where: {owner_uuid: hero_uuid },
  });
  return result;
}

export async function createResourceInstance(data) {
  const result = await prisma.resourceInstance.create({
    data,
  });
  return result;
}

export async function updateResourceInstance(uuid, data) {
  const result = await prisma.resourceInstance.update({
    where: {uuid},
    data,
  });
  return result;
}

export async function addResourcesToHero(owner_uuid, data) {
  const ownedResources = await prisma.resourceInstance.findMany({
    where: { owner_uuid }
  })
  for (const addedResource of data) {
    const indexAt = hasItemAtIndex(ownedResources, addedResource.blueprint, addedResource.quality);
    if (indexAt == -1) {
      const uuid = nanoid();
      await prisma.resourceInstance.create({data:{uuid, owner_uuid, ...addedResource}})
    } else {
      const ownedResource = ownedResources[indexAt];
      const newQuantity = addedResource.quantity + ownedResource.quantity;
      await prisma.resourceInstance.update({
        where: {uuid: ownedResource.uuid},
        data: {quantity: newQuantity}
      })
    }
  }
}

export async function destroyResourceInstance(uuid) {
  const result = await prisma.resourceInstance.delete({
    where: {uuid}
  });
  return result;
}


export async function destroyManyResourceInstances(uuids) {
  const result = await prisma.resourceInstance.deleteMany({
    where: {
      uuid: {
        in: uuids,
      },
    },
  });
  return result;
}