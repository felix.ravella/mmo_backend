import { nanoid } from 'nanoid';

import { createResourceInstance, destroyManyResourceInstances, destroyResourceInstance, updateResourceInstance } from '../repositories/resourceInstances.repository.js';

// POST /resources
export async function postResourceInstance(req, res) {
  try {
    let { blueprint, quantity, quality, owner_uuid} = req.body;
    if (!blueprint) {
      res.status(400).send('Incomplete post ResourceInstance request')
      return;
    }
    const uuid = nanoid();
    const resourceInstance = await createResourceInstance({uuid, ...req.body });
    res.status(200).send(resourceInstance);
    return;
  } catch (error) {
    console.log(error);
    res.status(500).send('Internal server error');
    return;
  }
}

// PUT /resources
export async function patchResourceInstance(req, res) {
  try {
    const result = await updateResourceInstance(req.params.uuid, req.body);
    res.status(200).send(result);
    return;
  } catch (error) {
    console.log(error);
    res.status(500).send('Internal server error');
    return;
  }
}


// DELETE /resources
export async function deleteResourceInstance(req, res) {
  try {
    const result = await destroyResourceInstance(req.params.uuid);
    res.status(200).send(result);
    return;
  } catch (error) {
    console.log(error);
    res.status(500).send('Internal server error');
    return;
  }
}