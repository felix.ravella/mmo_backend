import { nanoid } from 'nanoid';

import { findUserByUUID } from '../repositories/users.repository.js';
import { createHero, findHeroByName, findHeroByUUID, updateHero } from '../repositories/heroes.repository.js';
import { addStartingEquipables } from '../scripts/utils.js';

// GET /players/:uuid
export async function getPlayer(req, res) {
  const player = await findHeroByUUID(req.params.uuid);
  if (!player) {
    res.status(404).send('No player found')
    return;
  }
  res.status(200).send(player);
}

// POST /players
export async function postHero(req, res) {
  try {
    let { name, job, user_uuid } = req.body;
    if (!name || !job || !user_uuid) {
      res.status(400).send('Incomplete post Player request')
      return;
    }
    const existingUser = await findUserByUUID(user_uuid);
    if (!existingUser) {
      res.status(404).send('Tried to link to unknown user while creating Player')
      return;
    }
    const existingHero = findHeroByName(name);
    if (!existingHero) {
      res.status(404).send('This name is already taken')
      return;
    }
    const uuid = nanoid();
    const hero = await createHero({uuid, name, job, user_uuid});
    await addStartingEquipables(uuid);
    res.status(200).send(hero);
  } catch (error) {
    console.log(error);
    res.status(500).send('Internal server error');
  }
}

// UPDATE /players/:uuid
export async function patchHero(req, res) {
  try {
    const uuid = req.params.uuid;
    const payload = req.body;
    const hero = await updateHero(uuid, payload);
    res.status(200).send(hero);
  } catch (error) {
    console.log(error);
    res.status(500).send('Internal server error');
  }
}