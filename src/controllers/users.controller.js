import { nanoid } from 'nanoid';

import { createUser, findUserByLogin } from "../repositories/users.repository.js";
import { findHeroesByUserUUID } from '../repositories/heroes.repository.js';


// TODO
// GET /users/:uuid/players
export async function getUserPlayers(req, res) {
  try {
    const uuid = req.params.uuid;
    const heroes = await findHeroesByUserUUID(uuid);

    res.status(200).send(heroes);
    
  } catch (error) {
    console.log(error);
    res.status(500).send(error)
    return
  }
}

// POST /users
export async function postUser(req, res) {
  try {
    let { login, password } = req.body;
    if (!login || !password) {
      res.status(400).send('Incomplete post User request')
      return;
    }
    let existingAccount = await findUserByLogin(login);
    if (!!existingAccount) {
      res.status(409).send('Login is already taken')
      return;
    }
    const uuid = nanoid();

    const user = await createUser({ uuid, login, password })

    /*
    // Create token
    const token = jwt.sign(
      { userId: account.id },
      process.env.JWT_SECRET_KEY,
      {
        expiresIn: "30d",
      }
    );
    */
    res.status(200).send(user);

  } catch (error) {
    console.log(error);
    res.status(500).send(error)
    return
  }
}