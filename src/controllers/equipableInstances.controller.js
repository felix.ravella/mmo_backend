import { nanoid } from 'nanoid';

import { createEquipable } from "../repositories/equipableInstances.repository.js";

// POST /players
export async function postEquipableInstance(req, res) {
  try {
    let { blueprint, durability, quality,
      stat_0_value, stat_1_value, stat_2_value, stat_3_value,
      vanish_at, owner_uuid, crafter_uuid
    } = req.body;
    if (!blueprint) {
      res.status(400).send('Incomplete post EquipableInstance request')
      return;
    }
    const uuid = nanoid();
    const equipableInstance = await createEquipable({uuid, ...req.body });
    res.status(200).send(equipableInstance);
    return;
  } catch (error) {
    console.log(error);
    res.status(500).send('Internal server error');
    return;
  }
}