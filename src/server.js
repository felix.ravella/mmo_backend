import express from 'express';
import expressWS from 'express-ws';

import cors from 'cors';
import { getWs } from './routes/getWs.js';
import { getRoot } from './routes/getRoot.js';
import { postConnect } from './routes/postConnect.js';
import { getMap } from './routes/getMap.js';
import { setSpawnHordeTimer } from './scripts/spawnHorde.js';
import { usersRoute } from './routes/users.route.js';
import { heroesRoute } from './routes/heroes.route.js';
import { getSeed } from './routes/getSeed.js';
import { equipableInstancesRoute } from './routes/equipableInstances.route.js';
import { resourceInstancesRoute } from './routes/resourceInstances.route.js';
import { postSell } from './routes/postSell.route.js';


function main() {
  const app = express();
  expressWS(app);

  app.use(cors());

  app.use((req, res, next) => {
    //console.log(new Date().toISOString(), req.method, req.path);
    next();
  });

  const sockets = new Map();
  
  const maps = {
    "playground": {players: new Map(), hordes: new Map(), battles: new Map()},
    "01": {players: new Map(), hordes: new Map(), battles: new Map()},
    "02": {players: new Map(), hordes: new Map(), battles: new Map()},
    "10,10": {players: new Map(), hordes: new Map(), battles: new Map()},
    "10,11": {players: new Map(), hordes: new Map(), battles: new Map()},
    "10,12": {players: new Map(), hordes: new Map(), battles: new Map()},
    "10,13": {players: new Map(), hordes: new Map(), battles: new Map()},
    "11,10": {players: new Map(), hordes: new Map(), battles: new Map()},
    "11,11": {players: new Map(), hordes: new Map(), battles: new Map()},
    "11,12": {players: new Map(), hordes: new Map(), battles: new Map()},
    "11,13": {players: new Map(), hordes: new Map(), battles: new Map()},
    "12,10": {players: new Map(), hordes: new Map(), battles: new Map()},
    "12,11": {players: new Map(), hordes: new Map(), battles: new Map()},
    "12,12": {players: new Map(), hordes: new Map(), battles: new Map()},
    "12,13": {players: new Map(), hordes: new Map(), battles: new Map()},
    "13,10": {players: new Map(), hordes: new Map(), battles: new Map()},
    "13,11": {players: new Map(), hordes: new Map(), battles: new Map()},
    "13,12": {players: new Map(), hordes: new Map(), battles: new Map()},
    "13,13": {players: new Map(), hordes: new Map(), battles: new Map()},
    "14,11": {players: new Map(), hordes: new Map(), battles: new Map()},
    "14,12": {players: new Map(), hordes: new Map(), battles: new Map()},
  }

  // routes
  getRoot(app);
  getSeed(app);
  usersRoute(app);
  heroesRoute(app);
  equipableInstancesRoute(app);
  resourceInstancesRoute(app);
  getWs(app, sockets, maps);
  postConnect(app);
  getMap(app, maps);
  postSell(app);


  // error handling
  /*
  app.use((error: Error, req: Request, res: Response) => {
    console.log('err')
    console.error(error);
    res.status(500).send('Internal server error');
  })
  */
  const port = process.env.PORT || 3000;

  app.listen(port, () => {
    console.log('\nServer is runnning on port 3000')
    // launch recurring spawn loop
    
    setSpawnHordeTimer(maps, sockets)
  });


}

main();